# Foutmelding
Ik deed dit / Ik probeerde dit, maar ik verwachte dat ...


## Stappenplan
- [ ] Vul in wat voor omgeving je zit (https://www.whatsmybrowser.org/)
- [ ] Gebeurt het ook als je niet ingelogd bent? (als mogelijk)
- [ ] Vul stappenplan in
- [ ] Voeg screenshots toe

## Omgeving 
- whatsmybrowser.org/...


## Stappen om het te reproduceren
- [ ] Ik ga naar https://
- [ ] Ik klik op, vul tekst in als dit etc.etc.
