## Git commit log
FROM docker.io/alpine/git:v2.30.2 as git-stage
WORKDIR /app
COPY /.git /.git
RUN git log --no-merges --date=short --pretty="%cd %s" > ./commits.txt

# Build the app!
FROM docker.io/hyven/deves as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . .
RUN chmod 777 ./updateLang.sh && $SHELL ./updateLang.sh
RUN npm run build

# Lets go to production stage
FROM nginx:1.21-alpine as production-stage
COPY --from=git-stage /app/commits.txt /usr/share/nginx/html/assets/changelog/commits.txt
COPY --from=build-stage /app/build /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
