# Hyven Fronten Development
[![coverage report](https://gitlab.com/hyven/frontend/badges/main/coverage.svg)](https://gitlab.com/hyven/frontend/-/commits/main)
[![pipeline status](https://gitlab.com/hyven/frontend/badges/%2338-development/pipeline.svg)](https://gitlab.com/hyven/frontend/-/commits/main)

> Hyven frontend based on [svelte](https://svelte.dev/)  
> Project maintained with the [kit](https://kit.svelte.dev/)

## deves
> [Deves](https://gitlab.com/hyven/deves) is a docker container made for frontend development. It ensures that we are all on the same node version with the correct tools installed.

**Development**
```bash
docker run -it --rm -p 2304:2304 -p 24678:24678 --mount "type=bind,src=${PWD},dst=/deves/local" hyven/deves -nh -ng dev
```
**Building**
```bash
docker run -it --rm -p 2304:2304 -p 24678:24678 --mount "type=bind,src=${PWD},dst=/deves/local" hyven/deves -nh -ng build
```

## PLEH
`help` You should [read the docs](https://svelte.dev/docs) before starting svelte  
But since I know myself, you won't probably so here is a cheat sheet.

**https://svelte.dev/examples Look at the examples, they are great!**

### Script [[docs](https://svelte.dev/docs#script)]
```html
<script lang="ts">
    // code is for every instance of this element
    export const thisIsAPropertyThatYouCanUseWithinHTML = 1;
    const thisIsAInternalVariable = 2;
    $: thisIsACalculatedProperty = thisIsAPropertyThatYouCanUseWithinHTML + thisIsAInternalVariable; 
</script>
<script context="module" lang="ts">
    // code is static / singleton, will run only once
    // `import { globalProperty } from './Filename.svelte'`
    export const globalProperty = 1;
    
</script>
```

### Binding 
Reference an HTML element [[docs](https://svelte.dev/docs#bind_element)]
```html
<script>
	import { onMount } from 'svelte';

	let canvasElement;
	onMount(() => {
		const ctx = canvasElement.getContext('2d');
		// ... do stuff with ctx
	});
</script>

<canvas bind:this={canvasElement}></canvas>
```
Classes [[docs](https://svelte.dev/docs#class_name)]
```html
<div 
        <!-- if statement is true assign class isTrue else isFalse -->
        class="{statement ? 'isTrue' : 'isFalse'}"
        <!-- iLike is applied if iLike is truthy -->
        class:iLike={iLike}
        <!-- inactive is applied if iLike is falsy -->
        class:inactive={!active}
></div>
```
Events [[docs](https://svelte.dev/docs#on_component_event)]
```html
<SomeComponent on:whatever={handler}/>
<SomeComponent on:whatever/>
```
Properties [[docs](https://svelte.dev/docs#bind_component_property)]
```html
<Keypad bind:value={pin}/>
```
