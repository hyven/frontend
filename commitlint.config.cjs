module.exports = {
	parserPreset: {
		'parserOpts': {
			'headerPattern': /^\[#\d+]\s(\w*):(.+)$/, // `[#id] type: subject`
			'headerCorrespondence': ['type', 'subject'],
		}
	},
	helpUrl: '[#<IdOfIssue>] <feat|fix|docs|style|refactor|test|chore>: <message>',
	extends: [
		'@commitlint/config-conventional'
	],
	rules: {
		'body-full-stop': [2, 'never'],
		'header-case': [2, 'never', 'upper-case'],
		'header-full-stop': [2, 'never'],
		'header-min-length': [2, 'always', 10],
		'header-max-length': [2, 'always', 72],
		'subject-min-length': [2, 'always', 4],
		'type-enum': [2, 'always', [
			'feat',
			'fix',
			'docs',
			'style',
			'refactor',
			'test',
			'chore',
		]],
		'type-empty': [2, 'never'],
		'type-case': [2, 'always', ['lower-case', 'upper-case']],
	}
};
